﻿

CREATE TABLE userTable(userName VARCHAR (50),email VARCHAR(30) NOT NULL , pass VARCHAR(20) NOT NULL ,xLocation float(10),yLocation float(10), mac varchar(40) , timeS datetime,CONSTRAINT user_pk PRIMARY KEY (email));
CREATE TABLE messageFromUser(email VARCHAR(30), messageText VARCHAR(100),timeS datetime,CONSTRAINT message_pk PRIMARY KEY (email,messageText,timeS) ,FOREIGN KEY (email) REFERENCES userTable(email) ON DELETE CASCADE );
CREATE TABLE admins(id VARCHAR(20) NOT NULL, pass VARCHAR(20) NOT NULL,adminName VARCHAR(30),email VARCHAR(30),CONSTRAINT admin_pk PRIMARY KEY(id));
CREATE TABLE sendingTime( timeS datetime, constraint sending_pk primary key(timeS));
CREATE TABLE userPhotos(email VARCHAR(30),photo image,CONSTRAINT user_photo_pk PRIMARY KEY(email),FOREIGN KEY(email) REFERENCES userTable(email) ON DELETE CASCADE);
CREATE TABLE userAllergies(email VARCHAR(30),allergieName VARCHAR(40),CONSTRAINT user_allergies_pk PRIMARY KEY(email,allergieName),FOREIGN KEY(email) REFERENCES userTable(email) ON DELETE CASCADE);
CREATE TABLE userChronics(email VARCHAR(30),chronicName VARCHAR(60),CONSTRAINT user_chronics_pk PRIMARY KEY(email,chronicName),FOREIGN KEY(email) REFERENCES userTable(email) ON DELETE CASCADE);
CREATE TABLE userTokens(email VARCHAR(30),tokenString VARCHAR(100),endDate datetime,CONSTRAINT token_pk PRIMARY KEY(email,tokenString),FOREIGN KEY(email) REFERENCES userTable(email) ON DELETE CASCADE);
CREATE TABLE userNearbys(email VARCHAR(30),nearbyMail VARCHAR(30),recordTime datetime,CONSTRAINT nearby_pk PRIMARY KEY(email,nearbyMail),CONSTRAINT mail_fk FOREIGN KEY(email) REFERENCES userTable(email) ON DELETE CASCADE, CONSTRAINT nearby_fk FOREIGN KEY(nearbyMail) REFERENCES userTable(email) ON DELETE NO ACTION);
CREATE TABLE adminTokens(id VARCHAR(20),tokenString VARCHAR(100),endDate datetime,CONSTRAINT admin_token_pk PRIMARY KEY(id,tokenString),FOREIGN KEY(id) REFERENCES admins(id) ON DELETE CASCADE);




CREATE PROCEDURE AddToken
	@email VARCHAR(30),
	@tokenString VARCHAR(100),
	@endDate DateTime
AS
BEGIN TRANSACTION 
	INSERT INTO userTokens VALUES(@email,@tokenString,@endDate);
	COMMIT TRANSACTION 
RETURN 0 ;

CREATE PROCEDURE AddAdminToken
	@id VARCHAR(20),
	@tokenString VARCHAR(100),
	@endDate DateTime
AS
BEGIN TRANSACTION 
	INSERT INTO adminTokens VALUES(@id,@tokenString,@endDate);
	COMMIT TRANSACTION 
RETURN 0 ;

CREATE PROCEDURE CheckAdminToken
	@tokenString VARCHAR(100)
AS
BEGIN 
	SELECT * FROM adminTokens WHERE tokenString = @tokenString AND endDate > sysdatetime();
END
RETURN 0;

CREATE PROCEDURE CheckToken
	@tokenString VARCHAR(100)
AS
BEGIN 
	SELECT * FROM userTokens WHERE tokenString = @tokenString AND endDate > sysdatetime();
END
RETURN 0;

CREATE PROCEDURE GetAdminMail
	@id VARCHAR(20)
AS
BEGIN 
	SELECT email FROM admins WHERE id=@id;
END
RETURN 0;

drop procedure GetAdminPassword

CREATE PROCEDURE GetUserNearbys
	@email VARCHAR(30)
AS
BEGIN 
	SELECT * FROM userNearbys u WHERE u.email = @email or u.nearbyMail = @email;
END
RETURN 0 ;

CREATE PROCEDURE GetUserNearbysByName
	@name VARCHAR(50)
AS
BEGIN 
	SELECT * FROM userNearbys u WHERE u.email in (select email from usertable where userName = @name) or u.nearbyMail in (select email from usertable where userName = @name);
END
RETURN 0 ;

drop procedure GetUserNearbys

CREATE PROCEDURE ClearUserNearbys
	@email VARCHAR(30)
AS
BEGIN TRANSACTION 
	DELETE FROM userNearbys WHERE email = @email;
	COMMIT TRANSACTION 
RETURN 0 ;


CREATE PROCEDURE AddNearby
	@email VARCHAR(30),
	@nearbyMAC VARCHAR(40),
	@time DATETIME
AS
BEGIN TRANSACTION 
	IF (@email not in (select email from userNearbys) OR @email not in (select nearbyMail from userNearbys))
		INSERT INTO userNearbys VALUES(@email,(select email from usertable where mac = @nearbyMAC),@time);
		COMMIT TRANSACTION
	
RETURN 0 ;

drop procedure AddNearby

CREATE PROCEDURE AddAdmin
	@id VARCHAR(20),
	@pass VARCHAR(20),
	@name VARCHAR(30),
	@mail VARCHAR(30)
AS
BEGIN TRANSACTION 
	INSERT INTO admins VALUES(@id,@pass,@name,@mail);
	COMMIT TRANSACTION 
RETURN 0 ;

drop procedure AddAdmin

CREATE PROCEDURE AddUser
	@email VARCHAR(30),
	@pass VARCHAR(20),
	@name VARCHAR(50),
	@bloodType VARCHAR(10)
AS
BEGIN TRANSACTION 
	INSERT INTO userTable VALUES(@name,@email,@pass,0.0,0.0,'',NULL,@bloodType);
	COMMIT TRANSACTION 
RETURN 0 ;


CREATE PROCEDURE UpdateBloodType
	@email VARCHAR(30),
	@bloodType VARCHAR(10)
AS
BEGIN TRANSACTION 
	UPDATE userTable SET bloodType = @bloodType WHERE email = @email;
	COMMIT TRANSACTION 
RETURN 0 ;

CREATE PROCEDURE AddPhoto
	@email VARCHAR(30),
	@photo image
AS
BEGIN TRANSACTION 
	INSERT INTO userPhotos VALUES(@email,@photo);
	COMMIT TRANSACTION ;
RETURN 0;

CREATE PROCEDURE AddAllergie
	@email VARCHAR(30),
	@allergie VARCHAR(40)
AS
BEGIN TRANSACTION 
	INSERT INTO userAllergies VALUES(@email,@allergie);
	COMMIT TRANSACTION ;
RETURN 0;

CREATE PROCEDURE DeleteAllergie
	@email VARCHAR(30),
	@allergie VARCHAR(40)
AS
BEGIN TRANSACTION 
	DELETE FROM userAllergies where email = @email AND allergieName = @allergie;
	COMMIT TRANSACTION ;
RETURN 0;

CREATE PROCEDURE AddChronic
	@email VARCHAR(30),
	@chronic VARCHAR(60)
AS
BEGIN TRANSACTION 
	INSERT INTO userChronics VALUES(@email,@chronic);
	COMMIT TRANSACTION ;
RETURN 0;



CREATE PROCEDURE DeleteChronic
	@email VARCHAR(30),
	@chronic VARCHAR(60)
AS
BEGIN TRANSACTION 
	DELETE FROM userChronics where email = @email AND chronicName = @chronic;
	COMMIT TRANSACTION ;
RETURN 0;


CREATE PROCEDURE UpdatePhoto
	@email VARCHAR(30),
	@photo image
AS
BEGIN TRANSACTION 
	UPDATE  userPhotos SET photo = @photo WHERE email=@email;
	COMMIT TRANSACTION ;
RETURN 0;

CREATE PROCEDURE GetUserPhotos
@email varchar(30)

AS
BEGIN
	select photo from userPhotos where email = @email;
END
RETURN 0 ;

CREATE PROCEDURE AddMessage
	@email VARCHAR(30),
	@message VARCHAR(100)
AS
BEGIN TRANSACTION 
	INSERT INTO messageFromUser VALUES(@email,@message,sysdatetime());
	COMMIT TRANSACTION ;
RETURN 0;



CREATE PROCEDURE AddUserLocation
	@email VARCHAR(30),
	@xLocation float(10),
	@yLocation float(10),
	@timeS datetime
AS
BEGIN TRANSACTION 
	UPDATE userTable SET xLocation = @xLocation, yLocation = @yLocation,timeS = @timeS WHERE email = @email;
	COMMIT TRANSACTION 
RETURN 0 ;

CREATE PROCEDURE GetAllUsersAndLocations
AS
BEGIN
select UserName=userName,Latitude=xLocation,Longitude=yLocation from userTable
END
RETURN 0 ;


CREATE PROCEDURE GetUserAllergies
	@email VARCHAR(30)
AS
BEGIN
select allergieName from userAllergies where email = @email;
END
RETURN 0 ;

CREATE PROCEDURE GetUserChronics
	@email VARCHAR(30)
AS
BEGIN
select chronicName from userChronics where email = @email;
END
RETURN 0 ;


CREATE PROCEDURE GetAllUsersAtAreaWithCoordinate
@latitude float(10),
@longitude float(10),
@radius float(10)
AS
BEGIN
	select UserName = userName,Latitude = xLocation,Longitude=yLocation from userTable where xLocation < (@latitude + (@radius/2)) 
														and xLocation > (@latitude - (@radius/2))
														and yLocation < (@longitude + (@radius/2))
														and yLocation > (@longitude - (@radius/2));
END
RETURN 0 ;



CREATE PROCEDURE AddUserLocationAndMac
	@email VARCHAR(30),
	@xLocation float(10),
	@yLocation float(10),
	@timeS datetime,
	@mac varchar(40)
AS
BEGIN TRANSACTION 
	UPDATE userTable SET xLocation = @xLocation, yLocation = @yLocation,mac = @mac ,timeS =  @timeS WHERE email = @email;
	COMMIT TRANSACTION 
RETURN 0 ;



drop procedure GetAllUsersAndLocations

CREATE PROCEDURE AddUserMac
	@email VARCHAR(30),
	@mac varchar(40)
AS
BEGIN TRANSACTION 
	UPDATE userTable SET mac = @mac WHERE email = @email;
	COMMIT TRANSACTION 
RETURN 0 ;


CREATE PROCEDURE AddSendingTime
	@timeS datetime
AS
BEGIN TRANSACTION 
	INSERT INTO sendingTime VALUES(@timeS);
	COMMIT TRANSACTION 
RETURN 0 ;

SELECT * FROM usertable;
SELECT * FROM messageFromUser;
DELETE FROM usertable ;
DELETE FROM messagefromuser;

drop table messagefromuser;
drop table usertable;



select * from sendingTime;





///////////////////////////////////////////SQL FOR PROCEDURES///////////////////////

create table proceduresTable(procedureID int IDENTITY(0,1) , procedureString varchar(100),procedureName varchar(50),constraint procedure_pk primary key(procedureID));
create table pParameters(procedureID int,parameterID varchar(30) ,parameterOrder int,parameterName varchar(30),parameterType INT,constraint parameter_pk primary key(procedureID,parameterOrder),constraint parameter_fk foreign key(procedureID) references proceduresTable(procedureID) on delete cascade);
create table properColNames(procedureID int,colNumber int, colName varchar(30), constraint properColName_pk primary key(procedureID,colNumber),constraint properColName_fk foreign key(procedureID) references proceduresTable(procedureID) on delete cascade);



insert into proceduresTable values('GetUserNearbysByName','Get User Nearbys By Name');
insert into pParameters values(10,'name',1,'Name',0);
insert into pParameters values(8,'longitude',2,'Longitude',5);
insert into pParameters values(8,'radius',3,'Radius',5);



select * from properColNames


select * from pParameters
select * from proceduresTable


