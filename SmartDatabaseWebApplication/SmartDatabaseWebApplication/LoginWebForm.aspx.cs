﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using SmartDatabaseWebApplication.Classes;

namespace SmartDatabaseWebApplication
{
    public partial class AreYouSafeWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void LogButton_Click(object sender, EventArgs e)
        {

            SmartDatabaseService service = new SmartDatabaseService();


            if(tokenInput.Text.Trim().Length > 0)
            {
                if (service.CheckAdminToken(tokenInput.Text.Trim()))
                {
                    Response.Redirect("QueryWebForm.aspx");
                }
                else
                {
                    MessageBoard.Text = "Invalid Token !";
                }
            }

            else
            {
                MessageBoard.Text = "Please Enter a Token !";
            }

            
            
        }

        protected void CreateToken_Click(object sender, EventArgs e)
        {

            SmartDatabaseService service = new SmartDatabaseService();

            if (idInput.Text.Length == 0)
            {
                MessageBoard.Text = "Please Enter ID !";
            }

            else if (passInput.Text.Length == 0)
            {
                MessageBoard.Text = "Please Enter Password !";
            }

            else
            {
                if (service.IsAdminApproved(idInput.Text.Trim(), passInput.Text.Trim()))
                {

                    new AdminControl().CreateAdminToken(idInput.Text.Trim(), passInput.Text.Trim());

                    MessageBoard.Text = "Please Check Your Mail for the Token";
                }
                else
                {
                    MessageBoard.Text = "Wrong Credentials! ";
                }
            }
        }
    }
}