﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QueryWebForm.aspx.cs" Inherits="SmartDatabaseWebApplication.QueryWebForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>
<html>
  <head runat="server">
    <title>Admin Panel</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
   
  </head>
  <body>
     
    
    <div id="map" style="border-style: solid; border-color: inherit; border-width: medium; height:557px; width:663px;">
       
    </div>
    

    <script>
        
        function initMap() {




            var temp = '<%=GetLocationsFromService()%>';
            setTimeout(function () { }, 1000);
            var locations = JSON.parse(temp);

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(35.24701, 33.02279),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });


            var infowindow = new google.maps.InfoWindow({});

            var marker, i;
            var markerList = [];
            for (i = 0; i < locations.length; i++) {
               
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                    map: map
                });
                
             
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                       infowindow.setContent(locations[i].name+" at "+locations[i].time);
                       infowindow.open(map, marker);
                    }
                })(marker, i));
                markerList.push(marker);
            }

            var option = { gridSize: 150, maxZoom: 20,imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'};
            var markerCluster = new MarkerClusterer(map, markerList,option );
               
        }

       
    </script>
     <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhA49ZpgwIwrCNhPzk2alCf38jyDiXCNA&libraries=places"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhA49ZpgwIwrCNhPzk2alCf38jyDiXCNA&callback=initMap"
    
    async defer></script>




      <form id="form1" runat="server">
           
            
            


          <div style="margin-top:40px;">
              
          </div>
          
          <div style="margin-top:40px;">
               
                <asp:ScriptManager ID='ScriptManager1' runat='server' EnablePageMethods='true' />
                <asp:LinkButton ID="procPanelControl" runat="server">Procedure Execution <</asp:LinkButton>
                <asp:Panel ID="procExecPanel" runat="server" BorderStyle="Solid" BorderColor="Black" BackColor="Window" Wrap="true" >
                    
                    <div style="margin-top:40px;">
                        <asp:Label ID="lblProcedures" runat="server" Text="Procedures"></asp:Label>
                        <asp:DropDownList runat="server"  AutoPostback="true" ID="procedureNamesDropDown" OnSelectedIndexChanged="procedureNamesDropDown_SelectedIndexChanged">
                
                        </asp:DropDownList>
                        <asp:Label ID="lnlParameters" runat="server"  style="margin-left:100px;"   Text="Required Parameters"></asp:Label>
                        <asp:TextBox ID="parametersText" runat="server" TextMode="MultiLine" Height="150px" Width="250px" ></asp:TextBox>
                    </div>
                    <div style="margin-top:40px;">
                        <asp:Label ID="lblInput" runat="server" Text="Inputs"></asp:Label>
                        <asp:TextBox ID="txtboxInput" runat="server" Width="618px" ToolTip="Enter your inputs seperated with commas (',')." ></asp:TextBox>
                    </div>
                    <div style="margin-top:5px;">
                        <asp:Button ID="executeButton" runat="server" Text="Execute" OnClick="executeButton_Click" />
                    </div>
                    <ajax:CollapsiblePanelExtender ID="procedurePanelCollapse" runat="server" TargetControlID="procExecPanel" ExpandControlID="procPanelControl" CollapseControlID="procPanelControl" TextLabelID="procPanelControl" CollapsedText="Procedure Execution >" ExpandedText="Procedure Execution <">
                       
                    </ajax:CollapsiblePanelExtender>
                 


                </asp:Panel>
          </div>

          <div style="margin-top:40px;">
              <asp:LinkButton ID="queryPanelControl" runat="server">Query Execution <</asp:LinkButton>
              <asp:Panel ID="queryPanel" runat="server" BorderStyle="Solid" BorderColor="Black" BackColor="Window" Wrap="true" ScrollBars="Auto" >
                    
                      <div>
                           <asp:TextBox ID="txtQueryText" runat="server" Width="100%" Height="140px" TextMode="MultiLine"></asp:TextBox>
                      </div>
                   
                    <div style="margin-top:5px;">
                        <asp:Button ID="btnQueryExec" runat="server" Text="Execute" OnClick="btnQueryExec_Click" />
                    </div>
                    <ajax:CollapsiblePanelExtender ID="queryPanelCollapse" runat="server" TargetControlID="queryPanel" ExpandControlID="queryPanelControl" CollapseControlID="queryPanelControl" TextLabelID="queryPanelControl" CollapsedText="Query Execution >" ExpandedText="Query Execution <">
                       
                    </ajax:CollapsiblePanelExtender>
                 


                </asp:Panel>
          </div>
                

          <div style="margin-top:40px;">
              <asp:LinkButton ID="resultPanelControl" runat="server">Result View <</asp:LinkButton>
              <asp:Panel ID="resultPanel" runat="server" BorderStyle="Solid" BorderColor="Black" BackColor="Window" Wrap="true" >
                    
                    
                    <asp:Table ID="tblResultTable" runat="server" BorderStyle="Solid" BackColor="PaleVioletRed" BorderColor="Black" Width="100%">

                    </asp:Table>
                    <ajax:CollapsiblePanelExtender ID="resultPanelCollapse" runat="server" TargetControlID="resultPanel" ExpandControlID="resultPanelControl" CollapseControlID="resultPanelControl" TextLabelID="resultPanelControl" CollapsedText="Result View >" ExpandedText="Result View <">
                       
                    </ajax:CollapsiblePanelExtender>
                 


                </asp:Panel>
          </div>

     
          

     </form>


  </body>
</html>
