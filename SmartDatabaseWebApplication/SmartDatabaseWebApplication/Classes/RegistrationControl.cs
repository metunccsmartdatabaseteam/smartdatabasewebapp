﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartDatabaseWebApplication.Classes
{
    public class RegistrationControl
    {

        #region System Variables
        private const int TokenTime = 3;
        #endregion

        public RegistrationControl() { }

        #region User Creation Methods
        public bool AddFullUser(string name, string mail, string password, string bloodType, string allergies, string chronics, string photo)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();
                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUser", out sc);
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = password;
                cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                var allergieList = allergies.Split(';');
                var chronicList = chronics.Split(';');


                for (int i = 0; i < allergieList.Length; i++)
                {
                    if (!AddAllergie(mail, allergieList[i])) return false;
                }

                for (int i = 0; i < chronicList.Length; i++)
                {
                    if (!AddChronic(mail, chronicList[i])) return false;

                }

                if (!AddPhoto(mail, photo)) return false;




                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        public bool AddUser(string name, string mail, string password, string bloodType)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();
                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUser", out sc);



                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = password;
                cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        #endregion

        #region Blood Type Methods
        public bool UpdateBloodType(string mail, string bloodType)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("UpdateBloodType", out sc);


                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        #endregion

        #region Photo 
        public bool AddPhoto(string mail, string photo)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddPhoto", out sc);

                var pht = System.Text.Encoding.Unicode.GetBytes(photo);

                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@photo", SqlDbType.Image).Value = pht;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool UpdatePhoto(string mail, string photo)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("UpdatePhoto", out sc);

                var pht = System.Text.Encoding.Unicode.GetBytes(photo);

                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@photo", SqlDbType.Image).Value = pht;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public string GetUserPhoto(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserPhotos", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;

                string photo = "";

                sc.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    photo = System.Text.Encoding.Unicode.GetString(((byte[])reader.GetValue(0)));
                }



                sc.Close();

                return photo;


            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion

        #region Allergie Methods
        public bool AddAllergie(string mail, string allergie)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddAllergie", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@allergie", SqlDbType.VarChar).Value = allergie;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool DeleteAllergie(string mail, string allergie)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("DeleteAllergie", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@allergie", SqlDbType.VarChar).Value = allergie;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }



        public string GetUserAllergies(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserAllergies", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return JsonConvert.SerializeObject(table);
            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion

        #region Chronic Methods
        public bool AddChronic(string mail, string chronic)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddChronic", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@chronic", SqlDbType.VarChar).Value = chronic;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool DeleteChronic(string mail, string chronic)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("DeleteChronic", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@chronic", SqlDbType.VarChar).Value = chronic;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public string GetUserChronics(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserChronics", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return JsonConvert.SerializeObject(table);
            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion

        #region Login Methods
        public bool IsRegistered(string mail)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select * from userTable where email = '" + mail + "'", out sc);


            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                sc.Close();
                return true;
            }



            sc.Close();
            return false;

        }

        public bool IsPasswordCorrect(string mail, string password)

        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select pass from userTable where email = '" + mail + "'", out sc);

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                if (reader.GetValue(0).ToString().Equals(password))
                {
                    sc.Close();
                    return true;
                }

            }
            sc.Close();
            return false;

        }
        #endregion

        #region Token
        public string CreateToken(string mail,string password)
        {

            string token = "";

            if(IsPasswordCorrect(mail,password))
            {
                try
                {
                    byte[] currentTime = BitConverter.GetBytes(DateTime.Now.ToBinary());
                    byte[] key = Guid.NewGuid().ToByteArray();
                    token = Convert.ToBase64String(currentTime.Concat(key).ToArray());


                    DatabaseConnection db = new DatabaseConnection();

                    SqlConnection sc;

                    SqlCommand cmd = db.GetProcedureCommand("AddToken", out sc);


                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                    cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = token;
                    cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = DateTime.Now.AddMonths(TokenTime);

                    sc.Open();
                    cmd.ExecuteNonQuery();
                    sc.Close();

                    return token;

                }
                catch (Exception e)
                {
                    return token;
                }


                

            }

            return token;


        }


        public bool CheckToken(string tokenString)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("CheckToken", out sc);

            cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = tokenString; 

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                sc.Close();
                return true;

            }
            sc.Close();
            return false;

        }


        #endregion





    }
}