﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net.Mail;

namespace SmartDatabaseWebApplication.Classes
{
    public class Utilities
    {

        #region Message Control

        public bool AddMessage(string mail, string message)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddMessage", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;

                cmd.CommandType = CommandType.StoredProcedure;

                sc.Open();
                cmd.ExecuteNonQuery();
                sc.Close();
                return true;
            }
            catch
            {
                
                return false;
            }
        }

        #endregion

        #region Mail
        public void SendMail(string adminMail,string content,string subject)
        {
            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = "Admin Panel Access Token";
                msg.Body = content;
                msg.From = new MailAddress("smartdatabaseMETU@gmail.com","Smart Database Service");
                msg.To.Add(adminMail);
                msg.IsBodyHtml = true;
                client.Host = "smtp.gmail.com";
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("smartdatabaseMETU", "METUSmartDatabase-*0");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(msg);
            }
            catch (Exception ex)
            {
               
            }

        }
        #endregion
    }
}