﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartDatabaseWebApplication.Classes
{
    public class AdminControl
    {

        #region System Variables
        private const double TokenTime = 1;
        #endregion

        #region HTML Methods
        public void GetProcedureNames(out List<string> procedureNames, out List<int> procedureIDs)
        {
            procedureNames = new List<string>();
            procedureIDs = new List<int>();
            try
            {
               

                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;


                SqlCommand cmd = db.GetProcedureText("select procedureName,procedureID from proceduresTable", out sc);



                SqlDataReader reader;

                sc.Open();

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {


                    procedureNames.Add(reader.GetValue(0).ToString());
                    procedureIDs.Add(int.Parse(reader.GetValue(1).ToString()));

                }

                sc.Close();
            }
            catch (Exception e)
            {

            }


        }

        public void GetParametersOf(string procID, out List<string> parameterNames, out List<string> parameterTypes)
        {
            parameterNames = new List<string>();
            parameterTypes = new List<string>();
            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;
                SqlCommand cmd = db.GetProcedureText("select parameterName,parameterType from pParameters where procedureID =" + procID.ToString() + " order by parameterOrder asc", out sc);


                SqlDataReader reader;

                sc.Open();

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {


                    parameterNames.Add(reader.GetValue(0).ToString());
                    parameterTypes.Add(GetDataTypeString(int.Parse(reader.GetValue(1).ToString())));

                }

                sc.Close();
            }
            catch(Exception e)
            {

            }
           



        }

        public DataTable ExecuteQuery(string query)
        {
            DataTable table = new DataTable();
            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;
                SqlCommand cmd = db.GetProcedureText(query, out sc);


                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                table = new DataTable("Result");

                sc.Open();

                adapter.Fill(table);

                sc.Close();

            }
            catch(Exception e)
            {
                return table;
            }

            return table;

        }
        #endregion

        #region Panel Execution Methods
        public string GetProcedureString(string procID)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select procedureString from proceduresTable where procedureId = " + procID, out sc);

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();

            string procString = null;

            if (reader.HasRows)
            {
                reader.Read();
                procString = reader.GetValue(0).ToString();

            }

            sc.Close();
            return procString;

        }
        public DataTable ExecuteProcedure(string procID, List<string> parameters)
        {


            string procedureString = GetProcedureString(procID);

            if (procedureString.Length > 0)
            {
                return ExecuteForProcedureString(procedureString, parameters, procID);
            }



            return new DataTable();
        }

        public DataTable ExecuteForProcedureString(string procedureString, List<string> parameters, string procID)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;
            SqlCommand cmd = db.GetProcedureCommand(procedureString, out sc);

            List<string> parameterNames;
            List<int> parameterTypes;

            GetParametersForExecution(procID, out parameterNames, out parameterTypes);

            for (int i = 0; i < parameterTypes.Count; i++)
            {
                SqlDbType dataType = GetDataType(parameterTypes[i]);
                cmd.Parameters.Add(parameterNames[i].ToString(), dataType).Value = parameters[i];
            }



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();


            sc.Open();
            adapter.Fill(table);
            sc.Close();

            return table;

        }

        public SqlDbType GetDataType(int typeNo)
        {
            switch (typeNo)
            {
                case 0: return SqlDbType.VarChar;
                case 1: return SqlDbType.Int;
                case 3: return SqlDbType.Char;
                case 4: return SqlDbType.DateTime;
                case 5: return SqlDbType.Float;
                case 6: return SqlDbType.DateTime;
                case 7: return SqlDbType.Bit;
                case 8: return SqlDbType.Binary;
                case 9: return SqlDbType.BigInt;
                case 10: return SqlDbType.Image;
            }

            return SqlDbType.VarChar;

        }

        public string GetDataTypeString(int typeNo)
        {
            switch (typeNo)
            {
                case 0: return SqlDbType.VarChar.ToString();
                case 1: return SqlDbType.Int.ToString();
                case 3: return SqlDbType.Char.ToString();
                case 4: return SqlDbType.DateTime.ToString();
                case 5: return SqlDbType.Float.ToString();
                case 6: return SqlDbType.DateTime.ToString();
                case 7: return SqlDbType.Bit.ToString();
                case 8: return SqlDbType.Binary.ToString();
                case 9: return SqlDbType.BigInt.ToString();
                case 10: return SqlDbType.Image.ToString();
            }

            return SqlDbType.VarChar.ToString();

        }
        public void GetParametersForExecution(string procID, out List<string> parameterNames, out List<int> parameterTypes)
        {

            parameterNames = new List<string>();
            parameterTypes = new List<int>();
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;
            SqlCommand cmd = db.GetProcedureText("select parameterID,parameterType from pParameters where procedureID =" + procID.ToString() + " order by parameterOrder asc", out sc);


            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            while (reader.Read())
            {


                parameterNames.Add(reader.GetValue(0).ToString());
                parameterTypes.Add(int.Parse(reader.GetValue(1).ToString()));

            }

            sc.Close();



        }
        #endregion

        #region Login Methods
        public bool AddAdmin(string id, string pass, string name,string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddAdmin", out sc);


                cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
                cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = pass;
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                cmd.Parameters.Add("@mail", SqlDbType.VarChar).Value = mail;



                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool IsAdmin(string id)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select * from admins where id = '" + id + "'", out sc);


            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                sc.Close();
                return true;
            }



            sc.Close();
            return false;

        }

        public bool IsAdminApproved(string id, string password)

        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select pass from admins where id = '" + id + "'", out sc);

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                if (reader.GetValue(0).ToString().Equals(password))
                {
                    sc.Close();
                    return true;
                }

            }
            sc.Close();
            return false;

        }

        public string GetAdminMail(string id)

        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("GetAdminMail", out sc);
            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                reader.Read();
                string mail = reader.GetValue(0).ToString();
                sc.Close();


                return mail;

            }
            sc.Close();
            return "";

        }
        #endregion

        #region Token
        public bool CreateAdminToken(string id, string password)
        {

            string token = "";

            if (IsAdminApproved(id, password))
            {
                try
                {
                    byte[] currentTime = BitConverter.GetBytes(DateTime.Now.ToBinary());
                    byte[] key = Guid.NewGuid().ToByteArray();
                    token = Convert.ToBase64String(currentTime.Concat(key).ToArray());


                    DatabaseConnection db = new DatabaseConnection();

                    SqlConnection sc;

                    SqlCommand cmd = db.GetProcedureCommand("AddAdminToken", out sc);


                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
                    cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = token;
                    cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = DateTime.Now.AddHours(TokenTime);

                    sc.Open();
                    cmd.ExecuteNonQuery();
                    sc.Close();

                    new Utilities().SendMail(GetAdminMail(id), "Your token to be used : " + token + " , will be expired in : " + TokenTime.ToString() + " Hour(s)", "Your Token to Login");


                    return true;

                }
                catch (Exception e)
                {
                    return false;
                }




            }

            return false;


        }


        public bool CheckAdminToken(string tokenString)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("CheckAdminToken", out sc);

            cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = tokenString;

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                sc.Close();
                return true;

            }
            sc.Close();
            return false;

        }


        #endregion

    }
}