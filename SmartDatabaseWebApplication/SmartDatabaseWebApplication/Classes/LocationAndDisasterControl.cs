﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartDatabaseWebApplication.Classes
{
    public class LocationAndDisasterControl
    {

        #region Location and MAC
        public bool AddUserLocation(string mail, string xLoc, string yLoc, DateTime t)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUserLocation", out sc);

                float xLocation = float.Parse(xLoc), yLocation = float.Parse(yLoc);

                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@xLocation", SqlDbType.Float).Value = xLocation;
                cmd.Parameters.Add("@yLocation", SqlDbType.Float).Value = yLocation;
                cmd.Parameters.Add("@timeS", SqlDbType.DateTime).Value = t;

                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public bool AddUserLocationAndMac(string mail, string xLoc, string yLoc, string mac, DateTime t)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUserLocationAndMac", out sc);

                float xLocation = float.Parse(xLoc), yLocation = float.Parse(yLoc);

                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@xLocation", SqlDbType.Float).Value = xLocation;
                cmd.Parameters.Add("@yLocation", SqlDbType.Float).Value = yLocation;
                cmd.Parameters.Add("@timeS", SqlDbType.DateTime).Value = t;
                cmd.Parameters.Add("@mac", SqlDbType.VarChar).Value = mac;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public bool AddUserMac(string mail, string mac)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUserMac", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@mac", SqlDbType.VarChar).Value = mac;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public string GetLocations()
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select lat=xLocation,lng=yLocation,name=userName,time = timeS from usertable", out sc);



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            sc.Open();

            adapter.Fill(table);

            sc.Close();

            return JsonConvert.SerializeObject(table, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });

        }
        #endregion

        #region Nearby
        public bool AddNearby(string mail, string nearbyMAC, DateTime time)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddNearby", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@nearbyMAC", SqlDbType.VarChar).Value = nearbyMAC;
                cmd.Parameters.Add("@time", SqlDbType.DateTime).Value = time;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool SetNearbys(string mail,string nearbyMACs, DateTime time)
        {

            try
            {

                ClearNearbys(mail);

                var nearbys = nearbyMACs.Split(';');
               



                for (int i = 0; i < nearbys.Length; i++)
                {
                    AddNearby(mail, nearbys[i], time);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ClearNearbys(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("ClearUserNearbys", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public string GetUserNearbys(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserNearbys", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return JsonConvert.SerializeObject(table);
            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion









    }
}