﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;

namespace SmartDatabaseWebApplication
{
    public partial class QueryWebForm : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProcedureNames();

               
            }
           
            
        }
       

        
        public static string GetLocationsFromService()
        {

            return new SmartDatabaseService().GetLocations();
        }

        private void LoadProcedureNames()
        {

            SmartDatabaseService service = new SmartDatabaseService();

            List<string> procedureNames;
            List<int> procedureIDs;

            service.GetProcedureNames(out procedureNames,out procedureIDs);
            procedureNamesDropDown.Items.Add("0");

            for (int i = 0; i< procedureNames.Count; i++)
            {
                procedureNamesDropDown.Items.Add(new ListItem(procedureNames[i], procedureIDs[i].ToString()));
            }
        }



        protected void procedureNamesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(procedureNamesDropDown.SelectedIndex != 0)
            {
                parametersText.Text = "";
                
                SmartDatabaseService service = new SmartDatabaseService();

                List<string> parameterNames;
                List<string> parameterTypes;

                service.GetParametersOf(procedureNamesDropDown.SelectedValue, out parameterNames, out parameterTypes);

                if (parameterNames.Count > 0)
                {
                    for (int i = 0; i < parameterNames.Count; i++)
                    {
                        parametersText.Text += (i + 1).ToString() + " - " + parameterNames[i] + " :  Type of = " + parameterTypes[i] + Environment.NewLine;
                    }
                }

                else
                {
                    parametersText.Text = "No parameters required.";
                }
                

               
            }
            
        }

        protected void executeButton_Click(object sender, EventArgs e)
        {
            if(procedureNamesDropDown.SelectedIndex != 0)
            {
                try
                {
                    var procedureID = procedureNamesDropDown.SelectedValue;
                    var procedureInputs = txtboxInput.Text.Split(',');

                    SmartDatabaseService service = new SmartDatabaseService();

                    DataTable table = service.ExecuteProcedure(procedureID, procedureInputs.ToList<string>());

                    FillResultTable(table);

                }
                catch (Exception exc)
                {

                }
                
            }
        }


        protected void btnQueryExec_Click(object sender, EventArgs e)
        {


             SmartDatabaseService service = new SmartDatabaseService();


             DataTable table = service.ExecuteQuery(txtQueryText.Text.ToString().Trim());
               
             FillResultTable(table);

        
            
        }

        private void FillResultTable(DataTable table)
        {

            tblResultTable.Rows.Clear();

            TableRow attributeNames = new TableRow();
            tblResultTable.Rows.Add(attributeNames);

            for (int j = 0; j < table.Columns.Count; j++)
            {
                TableCell cell = new TableCell();
                cell.BackColor = System.Drawing.Color.OrangeRed;
                cell.Text = table.Columns[j].ColumnName;
                attributeNames.Cells.Add(cell);
            }



            for (int i = 0; i < table.Rows.Count; i++)
            {
                TableRow row = new TableRow();
                tblResultTable.Rows.Add(row);

                for (int j = 0; j < table.Columns.Count; j++)
                {
                    TableCell cell = new TableCell();
                    cell.Text = table.Rows[i][j].ToString();
                    row.Cells.Add(cell);
                }
            }
        }
    }
}