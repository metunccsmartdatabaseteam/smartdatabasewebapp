﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginWebForm.aspx.cs" Inherits="SmartDatabaseWebApplication.AreYouSafeWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Are You Safe Admin Login</title>
</head>
<body>
    <form id="form1" runat="server">
        <br style ="margin-top=20px;"><br />
        <div>
             <h1 style="background-color:red;color:white;font-size:80px;text-align:center;">Log In</h1>
        </div>
      


        
        
        <div>
             <asp:Label ID="Label2" runat="server" Text="ID :" style ="font-size:30px;margin-right:80px;"></asp:Label>
            <asp:TextBox ID="idInput" runat="server"  style="margin-left:85px;" Height="30px" Width="280px"></asp:TextBox>
        </div>
           
        <br style ="margin-top=20px;"><br />
         <div>
              <asp:Label ID="Label3" runat="server" Text="Password :" style ="font-size:30px;margin-right:80px;"></asp:Label>
                <asp:TextBox ID="passInput" runat="server" Height="30px" Width="280px"  input type="password"></asp:TextBox>
         </div>   

        <br style ="margin-top=20px;"><br />
         <div>
              <asp:Label ID="lblToken" runat="server" Text="Token :" style ="font-size:30px;margin-right:120px;"></asp:Label>
                <asp:TextBox ID="tokenInput" runat="server" Height="30px" Width="280px"  input type="password"></asp:TextBox>
         </div>  
       
        <div>
            <asp:Button ID="tokenButton" runat="server" Text="Get Token" style="align-items:center;align-self:center;margin-left:553px;" Height="54px" Width="158px" OnClick="CreateToken_Click" />
            <asp:Button ID="loginButton" runat="server" Text="Login" style="align-items:center;align-self:center;" Height="54px" Width="158px" OnClick="LogButton_Click" />
        </div>

         <br style ="margin-top=20px;"><br />
        <div>
            <asp:TextBox ID="MessageBoard" runat="server" Height="74px" Width="1245px" Font-Bold="True" ForeColor="#CC0000"></asp:TextBox> 
        </div>
    </form>
</body>
</html>
