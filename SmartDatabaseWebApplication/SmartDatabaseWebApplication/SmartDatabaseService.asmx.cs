﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SmartDatabaseWebApplication.Classes;

namespace SmartDatabaseWebApplication
{
    /// <summary>
    /// Summary description for SmartDatabaseService
    /// </summary>
    [WebService(Namespace = "http://smartdatabasewebservice.metu.edu.tr/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SmartDatabaseService : System.Web.Services.WebService
    {

        #region Registration Methods
        [WebMethod]
        public bool AddFullUser(string name, string mail, string password, string bloodType, string allergies, string chronics, string photo)
        {
            return new RegistrationControl().AddFullUser(name, mail, password, bloodType, allergies, chronics, photo);

        }

        [WebMethod]
        public bool AddUser(string name, string mail, string password, string bloodType)
        {

            return new RegistrationControl().AddUser(name, mail, password, bloodType);

        }

        [WebMethod]
        public bool UpdateBloodType(string mail, string bloodType)
        {

            return new RegistrationControl().UpdateBloodType(mail, bloodType);

        }

        [WebMethod]
        public bool AddPhoto(string mail, string photo)
        {

            return new RegistrationControl().AddPhoto(mail, photo);

        }

        [WebMethod]
        public bool UpdatePhoto(string mail, string photo)
        {

            return new RegistrationControl().UpdatePhoto(mail, photo);

        }

        [WebMethod]
        public string GetUserPhoto(string mail)
        {

            return new RegistrationControl().GetUserPhoto(mail);

        }

        [WebMethod]
        public bool AddAllergie(string mail, string allergie)
        {

            return new RegistrationControl().AddAllergie(mail, allergie);

        }

        [WebMethod]
        public bool DeleteAllergie(string mail, string allergie)
        {

            return new RegistrationControl().DeleteAllergie(mail, allergie);

        }

        [WebMethod]
        public string GetUserAllergies(string mail)
        {

            return new RegistrationControl().GetUserAllergies(mail);

        }

        [WebMethod]
        public bool AddChronic(string mail, string chronic)
        {

            return new RegistrationControl().AddChronic(mail, chronic);

        }

        [WebMethod]
        public bool DeleteChronic(string mail, string chronic)
        {

            return new RegistrationControl().DeleteChronic(mail, chronic);

        }

        [WebMethod]
        public string GetUserChronics(string mail)
        {

            return new RegistrationControl().GetUserChronics(mail);
        }

        [WebMethod]
        public bool IsRegistered(string mail)
        {
            return new RegistrationControl().IsRegistered(mail);
        }


        [WebMethod]
        public bool IsPasswordCorrect(string mail, string password)

        {
            return new RegistrationControl().IsPasswordCorrect(mail, password);
        }
        #endregion

        #region Location And Disaster Control
        [WebMethod]
        public bool AddNearby(string mail, string nearbyMail, DateTime time)
        {
            return new LocationAndDisasterControl().AddNearby(mail, nearbyMail, time);
        }

        [WebMethod]
        public bool SetNearbys(string mail,string nearbyMACs,DateTime time)
        {
            return new LocationAndDisasterControl().SetNearbys(mail,nearbyMACs,time);
        }


        [WebMethod]
        public bool ClearNearbys(string mail)
        {
            return new LocationAndDisasterControl().ClearNearbys(mail);
        }

        [WebMethod]
        public string GetUserNearbys(string mail)
        {
            return new LocationAndDisasterControl().GetUserNearbys(mail);
        }
        [WebMethod]
        public bool AddUserLocation(string mail, string xLoc, string yLoc, DateTime t)
        {

            return new LocationAndDisasterControl().AddUserLocation(mail, xLoc, yLoc, t);
        }


        [WebMethod]
        public bool AddUserLocationAndMac(string mail, string xLoc, string yLoc, string mac, DateTime t)
        {

            return new LocationAndDisasterControl().AddUserLocationAndMac(mail, xLoc, yLoc, mac, t);
        }


        [WebMethod]
        public bool AddUserMac(string mail, string mac)
        {
            return new LocationAndDisasterControl().AddUserMac(mail, mac);
        }

        [WebMethod]
        public string GetLocations()
        {
            return new LocationAndDisasterControl().GetLocations();
        }
        #endregion

        #region Admin Methods
        [WebMethod]
        public bool AddAdmin(string id, string pass, string name,string mail)
        {

            return new AdminControl().AddAdmin(id, pass, name,mail);
        }

        [WebMethod]
        public bool IsAdmin(string id)
        {
            return new AdminControl().IsAdmin(id);
        }


        [WebMethod]
        public bool IsAdminApproved(string id, string password)
        {
            return new AdminControl().IsAdminApproved(id, password);
        }

        [WebMethod]
        public bool CreateAdminToken(string id ,string password)
        {
            return new AdminControl().CreateAdminToken(id, password);
        }

        [WebMethod]
        public bool CheckAdminToken(string token)
        {
            return new AdminControl().CheckAdminToken(token);
        }

        public void GetProcedureNames(out List<string> procedureNames, out List<int> procedureIDs)
        {

            new AdminControl().GetProcedureNames(out procedureNames, out procedureIDs);
        }



        public DataTable ExecuteProcedure(string procID, List<string> parameters)
        {

            return new AdminControl().ExecuteProcedure(procID, parameters);
        }



        public void GetParametersOf(string procID, out List<string> parameterNames, out List<string> parameterTypes)
        {

            new AdminControl().GetParametersOf(procID, out parameterNames, out parameterTypes);

        }




        public DataTable ExecuteQuery(string query)
        {

            return new AdminControl().ExecuteQuery(query);
        }


        #endregion

        #region Utilities

        [WebMethod]
        public bool AddMessage(string mail, string message)
        {
            return new Utilities().AddMessage(mail, message);
        }
        #endregion

        #region Token
        [WebMethod]
        public string CreateToken(string mail,string password)
        {
            return new RegistrationControl().CreateToken(mail, password);
        }
        
        [WebMethod]
        public bool CheckToken(string tokenString)
        {
            return new RegistrationControl().CheckToken(tokenString);
        }
        #endregion
    }



}